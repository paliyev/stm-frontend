import $ from 'jquery';

import '../scss/app.scss';

/* Swiper slider */
import './slider/components';

$('.open-dropdown').click(function(){
  $(this).next().toggleClass('close');
  $(this).toggleClass('closed-dropdown');
})
