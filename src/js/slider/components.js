/* Swiper slider items */
import bootstrap from 'bootstrap';
import { Swiper } from 'swiper/bundle';
import 'swiper/swiper-bundle.css';


/* Home Page */
var SwiperMain = new Swiper('.main-slider', {
  direction: 'vertical',
  slidesPerView: 1,
  loop: true,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
});
var galleryThumbs = new Swiper('.gallery-thumbs', {
  spaceBetween: 10,
  allowSlideNext:false,
  allowSlidePrev: false,
  slidesPerView: 1,
  watchSlidesVisibility: true,
  watchSlidesProgress: true,
  slideToClickedSlide: false
});
/* News Page Slider */
const SwiperNews = new Swiper('.news-slider', {
  slidesPerView: 1,
  loop: true,
  autoplay: {
    delay: 5000,
  },
  pagination: {
    el: '.swiper-pagination',
    clickable: true
  },
  thumbs: {
    swiper: galleryThumbs
  }
});



